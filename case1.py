import numpy as np
import matplotlib.pylab as plt

np.random.seed(3)


def computeQ(a, s, Q0=0):  # initial queue length is 0
    N = len(a)
    Q = np.empty(N)  # make a list to store the values of  Q
    Q[0] = Q0
    for n in range(1, N):
        Q[n] = max(Q[n - 1] + a[n] - s[n], 0)
    return Q


a = [0, 4, 7, 3, 1, 5, 0, 0, 0, ]
s = [3] * len(a)

Q = computeQ(a, s)
plt.plot(a, drawstyle='steps-pre', label='a')
plt.plot(s, drawstyle='steps-mid', label='s')
plt.plot(Q, drawstyle='steps-post', label='Q')
plt.legend()
plt.show()

A = np.cumsum(a)  # compute the total number of arrivals up to time n
D = A - Q
plt.plot(A, drawstyle='steps-pre', label='A')
plt.plot(D, drawstyle='steps-mid', label='D')
plt.plot(Q, drawstyle='steps-post', label='Q')
plt.legend()
plt.show()

a = np.random.poisson(3, size=10)
s = 3. * np.ones_like(a)
Q = computeQ(a, s)
A = np.cumsum(a)
D = A - Q
plt.plot(A, drawstyle='steps-pre', label='A')
plt.plot(D, drawstyle='steps-mid', label='D')
plt.plot(Q, drawstyle='steps-post', label='Q')
plt.legend()
plt.show()

a = np.random.poisson(3, size=20000)
s = 3. * np.ones_like(a)
Q = computeQ(a, s)
plt.plot(Q, drawstyle='steps-post', label='Q')
plt.legend()
plt.show()

a = np.random.poisson(3, size=20000)
s = 2.8 * np.ones_like(a)
Q = computeQ(a, s)
plt.plot(Q, drawstyle='steps-post', label='Q')
plt.legend()
plt.show()

a = np.random.poisson(3, size=20000)
s = 3.2 * np.ones_like(a)
Q = computeQ(a, s)
plt.plot(Q, drawstyle='steps-post', label='Q')
plt.legend()
plt.show()


def empericalDistribution(X):
    x = np.sort(X)
    # y = 1.*np.cumsum(np.ones_like(X))/len(X)
    y = np.linspace(1. / len(X), 1., num=len(X))
    return x, y


a = np.random.poisson(3, size=20000)
s = 3 * np.ones_like(a)
Q = computeQ(a, s)

x, F = empericalDistribution(Q)

plt.plot(x, F)
plt.show()

a = np.random.poisson(3, size=20000)
s = 3.2 * np.ones_like(a)
Q = computeQ(a, s)

x, F = empericalDistribution(Q)

plt.plot(x, F)
plt.show()

a = np.random.poisson(3, size=20000)

mu = 3.2
s = mu * np.ones_like(a)
Q = computeQ(a, s)
x, Fx = empericalDistribution(Q)

s = np.random.poisson(mu, size=20000)
Q = computeQ(a, s)
y, Fy = empericalDistribution(Q)

plt.plot(x, Fx, label="det service")
plt.plot(y, Fy, label="sto service")
plt.legend()
plt.show()

# np.random.seed(3)
a = np.random.poisson(11.8, 1000)


def unbalanced():
    p = np.empty([5, len(a)])
    p[0, :] = 1. * np.ones_like(a)
    p[1, :] = 1. * np.ones_like(a)
    p[2, :] = 1. * np.ones_like(a)
    p[3, :] = 3. * np.ones_like(a)
    p[4, :] = 9. * np.ones_like(a)
    return p


p = unbalanced()

for j in range(len(a)):
    Id = j % 5
    p[Id, j] = 0

s = np.sum(p, axis=0)

Qub = computeQ(a, s)


def balanced():
    p = np.empty([5, len(a)])
    p[0, :] = 2. * np.ones_like(a)
    p[1, :] = 2. * np.ones_like(a)
    p[2, :] = 3. * np.ones_like(a)
    p[3, :] = 4. * np.ones_like(a)
    p[4, :] = 4. * np.ones_like(a)
    return p


p = balanced()

for j in range(len(a)):
    Id = j % 5
    p[Id, j] = 0

s = np.sum(p, axis=0)
Qbb = computeQ(a, s)

p = unbalanced()
for j in range(int(len(a) / 5)):
    p[:, 5 * j] = 0
s = np.sum(p, axis=0)
Quu = computeQ(a, s)

p = balanced()
for j in range(int(len(a) / 5)):
    p[:, 5 * j] = 0
s = np.sum(p, axis=0)
Qbu = computeQ(a, s)

plt.plot(Qbb, label="bal cap, bal leave")
plt.plot(Qub, label="unbal capacity, bal leave")
plt.plot(Qbu, label="bal cap, unbal leave")
plt.plot(Quu, label="unbal capacity, unbal leave")
plt.legend(loc="upper left")  # , bbox_to_anchor=(1,1))
plt.show()


def computeQExtra(a, s, e, Q0=0):  #  initial queue length is 0
    N = len(a)
    Q = [0] * N  # make a list to store the values of  Q
    Q[0] = Q0
    for n in range(1, N):
        q = Q[n - 1]
        if q < 12:
            s[n] = max(s[n] - e, 0)  # protect from becoming negative
        elif q >= 24:
            s[n] += e
        Q[n] = max(q + a[n] - s[n], 0)
    return Q


a = np.random.poisson(11.8, 1000)
s = 12. * np.ones_like(a)
Q = computeQ(a, s)
Qe1 = computeQExtra(a, s, 1)
Qe2 = computeQExtra(a, s, 2)
Qe5 = computeQExtra(a, s, 5)

plt.plot(Q, label="Q")
plt.plot(Qe1, label="Qe1")
plt.plot(Qe2, label="Qe2")
plt.plot(Qe5, label="Qe5")
plt.legend()
plt.show()

np.random.seed(3)
a = np.random.poisson(11.8, 1000)
s = 12. * np.ones_like(a)
Q = computeQ(a, s)
Qe1 = computeQExtra(a, s, 1)
Qe2 = computeQExtra(a, s, 2)
Qe5 = computeQExtra(a, s, 5)

plt.plot(Q, label="Q")
plt.plot(Qe1, label="Qe1")
plt.plot(Qe2, label="Qe2")
plt.plot(Qe5, label="Qe5")
plt.legend()
plt.show()

np.random.seed(3)
a = np.random.poisson(11.8, 10000)
s = 12. * np.ones_like(a)
Q = computeQ(a, s)
Qe1 = computeQExtra(a, s, 1)
Qe2 = computeQExtra(a, s, 2)
Qe5 = computeQExtra(a, s, 5)

plt.plot(Q, label="Q")
plt.plot(Qe1, label="Qe1")
plt.plot(Qe2, label="Qe2")
plt.plot(Qe5, label="Qe5")
plt.legend()
plt.show()

a = np.random.poisson(11.8, 10000)
s = np.random.poisson(12, 10000)
Q = computeQ(a, s)
Qe1 = computeQExtra(a, s, 1)
Qe2 = computeQExtra(a, s, 2)
Qe5 = computeQExtra(a, s, 5)

plt.plot(Q, label="Q")
plt.plot(Qe1, label="Qe1")
plt.plot(Qe2, label="Qe2")
plt.plot(Qe5, label="Qe5")
plt.legend()
plt.show()

a = np.random.poisson(11.8, 10000)
s = np.random.poisson(12, 10000)
Q = computeQ(a, s)
Qe1 = computeQExtra(a, s, 1)
Qe2 = computeQExtra(a, s, 2)
Qe5 = computeQExtra(a, s, 5)

plt.plot(Q, label="Q")
plt.plot(Qe1, label="Qe1")
plt.plot(Qe2, label="Qe2")
plt.plot(Qe5, label="Qe5")
plt.legend()
plt.show()

np.random.seed(4)
a = np.random.poisson(11, 10000)
s = 12. * np.ones_like(a)
Qe1 = computeQExtra(a, s, 1)
Qe2 = computeQExtra(a, s, 2)
Qe5 = computeQExtra(a, s, 5)

plt.plot(Q, label="Q")
plt.plot(Qe1, label="Qe1")
plt.plot(Qe2, label="Qe2")
plt.plot(Qe5, label="Qe5")
plt.legend()
plt.show()

a = np.array([2, 3, 2, 1, 0, 0, 1, 0, 0])
A = np.cumsum(a)
Ainv = np.repeat(np.arange(len(a)), a)

s = [1] * len(a)
Q = computeQ(a, s)
D = A - Q

d = np.zeros(len(D), dtype=int)
d[1:] = np.diff(D)
Dinv = np.repeat(np.arange(len(d)), d)
print(Ainv)
print(Dinv)

f, ax = plt.subplots(2, sharex=True, sharey=True)
plt.xlim(0, 10)
plt.ylim(0, 10)
ax[0].plot(A, drawstyle='steps-post')
ax[0].plot(D, drawstyle='steps-post')
ax[1].plot(Ainv, drawstyle='steps-post', label="a")
ax[1].plot(Dinv, drawstyle='steps-post', label="d")
plt.legend()
plt.show()